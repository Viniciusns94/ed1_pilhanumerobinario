#include <stdio.h>
#include <stdlib.h>

#define MAX 20
typedef struct {
	int topo;
	int itens[MAX] ;
} pilha;

void inicia_pilha(pilha *p) {
	p->topo = -1;
}

int pilha_vazia(pilha p) {
	return p.topo == -1;
}

int pilha_cheia(pilha p) {
	return p.topo == MAX-1;
}

void empilha (pilha *p, int v) {
	if ( pilha_cheia(*p))
		puts("Pilha Cheia");
	else
		p->itens[++p->topo] = v;
}

int desempilha (pilha *p) {
	if (pilha_vazia(*p) )
		puts ("Pilha Vazia") ;
	else
		return p->itens[p->topo--];
}
void main ( ) {
	int num ;
	pilha P;
	scanf("%d",&num);
	fflush (stdin);
	
	inicia_pilha(&P) ;
	
	while (num) {
		empilha (&P,num%2);
		num=num/2;
	}
	while (!pilha_vazia(P))
		printf("%d",desempilha(&P));
	getchar();
	printf("\n");
}